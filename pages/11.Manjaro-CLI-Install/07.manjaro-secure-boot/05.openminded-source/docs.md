---
title: openminded-source
published: false
date: '07-02-2022 11:38'
taxonomy:
    category:
        - docs
---

openminded | 2022-02-07 00:27:09 UTC | #1

[details="Note"]
This tutorial is almost complete, it lacks only good formatting and one tiny addition which is not necessary but brings a bit of polish to the end result. Formatting, I'll do it in a couple of days. Sorry for that. I just feel that if I postpone publishing this again then I forget about it for weeks or even months (again) - that has already happened a couple of times :grinning_face_with_smiling_eyes:
By its nature this post is a response to the question once asked by @linux-aarhus and several other people. Hope you folks will find it useful.
PS: all commands were checked by me before posting. If you find anything being weird / unclear, do not hesitate to post and correct me.
[/details]

**Preface**: this tutorial is made with several assumptions in mind, which are listed below.

* Your PC has TPM2 device, which is `/dev/tpmrm0`.
* You have already installed your system on GPT-partitioned drive using UEFI mode.
* You have full disk encryption (FDE) already in place, meaning that your root (with /boot directory - no separate unencrypted boot partition is necessary), home and swap partitions are LVM2 volumes located inside LUKSv2 container (`/dev/sda2`), and the only unencrypted partition is EFI partition (`/dev/sda1`).
* Your EFI partition ($esp) size is 300 Mb or larger.
* The name of encrypted partition is `cryptlvm`. Adjust respective commands according to the name of yours.
* You have AUR enabled and yay installed, because some of tools are not available in official repo.

The explanation of how to do the above is out of scope of this tutorial, please refer to Arch Wiki's "LVM on LUKS" page for details. However it's totally possible to use whatever layout you want, e.g. BTRFS subvolumes inside LUKSv2 container or swapfile instead of partition, it's just the specific scenario I'm about to describe.

Before you begin, install all required tools:
`yay -S sbsigntools efitools tpm2-tools tpm2-totp-git plymouth-git sbupdate-git shim-signed`

The first step is enabling support for Secure Boot. Normally it should be disabled because if using its default settings it prevents Manjaro from booting, but as it is required for ensuring that only allowed code is executed, you need to enable it as follows:
`mkdir -p -v /etc/efikeys`
`chmod -v 700 /etc/efikeys`
`cd /etc/efikeys`

The following saves default Secure Boot keys just in case, but we're not going to change / remove them anyway so you can skip this step:
`efi-readvar -v PK -o default_PK.esl`
`efi-readvar -v KEK -o default_KEK.esl`
`efi-readvar -v db -o default_db.esl`
`efi-readvar -v dbx -o default_dbx.esl`

Now it's time to generate machine-specific key which will be used to authenticate your Manjaro kernels and bootloader(s):
`openssl req -new -x509 -newkey rsa:2048 -subj "/CN=$(hostname) platform key/" -keyout db.key -out db.crt -days 3650 -nodes -sha256`
`openssl x509 -outform DER -in db.crt -out db.cer`

Now you need to replace grub with systemd-boot to make whole setup easier and more straightforward. With extra effort you can keep Grub and use it as your bootloader but that again is out of scope of this how-to since it would overcomplicate things dramatically. Frankly speaking, systemd-boot isn't necessary too, but it makes booting different kernels and efi tools easier than just UEFI boot menu.
First, remove grub:
`pacman -R grub`

The following will change the mountpoint of your EFI partition to sane and logical location, which is, most importantly, perfectly supported by systemd-boot.
`umount /boot/efi`
`sed -i 's|/efi/boot|/efi|' /etc/fstab`
`mount -a`

Now let's copy our Secure Boot certificate on $ESP to let MokManager utility enroll it on the next boot:
`cp db.cer /efi/MOK.cer`

It's time to prepare initrd part of your boot image. You need to decide which path you prefer: udev-based initrd or systemd-based one. If you have no idea, choose the first (Manjaro/Arch default). Hereinafter everything hallmarked with (a) should be considered referring to udev-based initrd setup, and everything with a foregoing (b) literal -- referring to systemd-based setup.

Replace HOOKS array in your `/etc/mkinitcpio.conf` and save old ones commented in the end of the file just in case:
`OLD_HOOKS=$(cat /etc/mkinitcpio.conf | grep "^HOOKS=(")`
*a)*
`sed -i 's|'"${OLD_HOOKS}"'|HOOKS=(base udev keyboard consolefont autodetect plymouth modconf block plymouth-tpm2-totp tpm2 plymouth-encrypt lvm2 resume filesystems)|' /etc/mkinitcpio.conf && echo '#previous_'$OLD_HOOKS |tee -a /etc/mkinitcpio.conf`
***OR***
*b)*
`sed -i 's|'"${OLD_HOOKS}"'|HOOKS=(base systemd keyboard sd-vconsole autodetect sd-plymouth modconf block sd-plymouth-tpm2-totp sd-encrypt lvm2 filesystems)|' /etc/mkinitcpio.conf && echo '#previous_'$OLD_HOOKS |tee -a /etc/mkinitcpio.conf`
Do the following to add entry about your LUKS partition to crypttab (`discard` is optional):
`echo cryptlvm $(blkid -s UUID -o value /dev/sda2) - tpm2-device=/dev/tpmrm0,tpm2-pcrs=0+7+14,nofail,discard,x-initrd.attach | tee -a /etc/crypttab.initramfs`

Also for better (de)compression speed/efficiency ratio consider setting `COMPRESSION="zstd"` in your `/etc/mkinitcpio.conf` if not set already. Note though that this works only starting from linux 5.10.

At this stage you might want to check your cryptsetup status:
`cryptsetup luksDump /dev/sda2`
Normally you should have 1 or 2 keyslots already occupied, the first one is your passphrase, the other is probably `/crypto_keyfile.bin`, and no tokens in place (yet). I suggest nuking your slot 1 if it's occupied by `crypto_keyfile` at the moment (since it's not safe to use crypto_keyfile in configuration we're building):
`cryptsetup luksKillSlot /dev/sda2 1`
Double-check, of course, I doubt you want to destroy a slot with actual passphrase.

This time you need to set up sbupdate config. It is a tool that generates signed and thus trusted kernel images used to boot your system. Issue
`cat /proc/cmdline > /etc/cmdline`
`GRUB_LEFTOVER=$(cat /etc/cmdline | grep "BOOT_IMAGE/" | cut -d' ' -f1)`
*a)*
`sed -i 's|'"{GRUB_LEFTOVER}"'|splash tpmkey=/dev/sda1:/keyfile:0x81000001 tpmpcr=sha1:0,7,14|' /etc/cmdline`
***OR***
*b)*
`sed -i 's|'"{GRUB_LEFTOVER}"'|splash|' /etc/cmdline`
`CMDLINE=$(cat /etc/cmdline)`
```
echo >>/etc/sbupdate.conf "
KEY_DIR="/etc/efikeys"
ESP_DIR="/efi"
OUT_DIR="EFI/Linux"
SPLASH="/sys/firmware/acpi/bgrt/image"
BACKUP=0
EXTRA_SIGN=('/efi/EFI/systemd/grubx64.efi')
CMDLINE_DEFAULT='$CMDLINE'"
```

The following will install systemd-boot and create a new boot entry for shim efi utility which will be used as a "bridge" between UEFI and Linux bootloader:
`OLD_MANJARO="$(efibootmgr | grep "manjaro" | cut -d' ' -f1 | cut -c 5-8)"`
`efibootgmr -b "${OLD_MANJARO}" -B`
`bootctl install && systemctl enable systemd-boot-update.service`
`cp /usr/share/shim-signed/* /efi/EFI/systemd/`
`cp /efi/EFI/systemd/systemd-bootx64.efi /efi/EFI/systemd/grubx64.efi`
`efibootgmr -c -d /dev/sda -p 1 -L "Manjaro" -l /EFI/systemd/shimx64.efi -v`
`mkinitcpio -P && sbupdate`
`BOOTORDER="$(efibootmgr | grep "^BootOrder: " | cut -d' ' -f2)"`
`efibootmgr --bootorder "${BOOTORDER}"`
By entering the last command above you'll have a new boot order where "Manjaro" (SB signed, actually `shimx64.efi` file) will be the first option, and "Linux Boot Manager" (no signature, actually `systemd-bootx64.efi` file) will be the 2nd one, auto-updateable efi binary which you might want from time to time to sign with sbsigntools or just manually copy it with resulting name of "grubx64.efi" and run `sbupdate` afterwards.

Okay, you are close to the final step now, but before that reboot, enter BIOS settings, set Secure Boot ON there, exit saving setting, and the next thing you see will be MokManager's blue screen (of death), where you should enroll your Secure Boot MOK certificate located on your $ESP.
When you finish enrolling, your PC will reboot and the next thing you will see will be systemd-boot menu. Choose any boot entry and enter your unlock passphrase. 

At last we've come to the most wanted part of this tutorial - time to set up your TPM2 module.
*a)*
Let's install luks-tpm2 tool and respective hook for mkinitcpio:
`yay -S luks-tpm2 mkinitcpio-tpm2-encrypt`
Then move luks-tpm2 alpm hook in order to avoid its triggering on kernel / bootloader update. Your TPM2 setup will rely on BIOS firmware, Secure Boot status and your MOK certificates check instead. 
`mv /usr/share/libalpm/hooks/luks-​tpm2.​hook /usr/share/libalpm/hooks/luks-​tpm2.​hook.​bak`
Time to make your TPM module aware of your persistence!
`tpm2_createprimary -c primary.ctx`
`tpm2_evictcontrol -c primary.ctx`
Now look at the output of the second command and write down the index of the persistent handle, it will be used later. I just assume it's `0x81000001`.
Edit `/etc/default/luks-tpm2`:
```
echo >>/etc/default/luks-tpm2 "
SEALED_KEY_PUBLIC="/efi/keyfile.pub"
SEALED_KEY_PRIVATE="/efi/keyfile.priv"
TPM2TOOLS_TCTI="device:/dev/tpmrm0"
ROOT_DEVICE="/dev/sda2"
PARENT_HANDLE="0x81000001"
PCRS="sha1:0,7,14"
UNSEAL_PCRS="sha1:0,7,14"
```
And add a TPM-sealed key to LUKS header:
`luks-tpm2 -p /efi/keyfile -H 0x81000001 /dev/sda2 init`
`mkinitcpio -P && sbupdate`
***OR***
*b)*
This is much less complicated, but only works properly since systemd v.250:
`systemd-cryptenroll /dev/sda3 --tpm2-device=/dev/tpmrm0 --tpm2-pcrs=0+7+14`

Reboot and enjoy. That's it.

PS: the reason why this howto does not involve checking kernel and cmdline PCRs is simple: Manjaro has oob support for variety of kernels, some may have unique cmdlines, so you probably do not want to enter your password every time you decide to boot another kernel. Relying on UEFI configuration + Secure Boot state + MOK list check and having all executables signed should be enough. Note though: we left systemd-bootx64.efi on $esp unsigned. When you decide to update it, do not copy it to grubx64.efi if unsure whether it was maliciously tampered with, copy it from /usr.
The rationale behind using sha1 is that it is more common but you are free to use sha256 or something else if your BIOS supports it.

**Sources**:
man sed
man bootctl
man systemd-cryptenroll
[Arch Wiki for systemd-boot](https://wiki.archlinux.org/title/Systemd-boot)
[Arch Wiki for TPM](https://wiki.archlinux.org/title/Trusted_Platform_Module)
[Arch Wiki for Secure Boot](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot)
[Luks-tpm2 GitHub page](https://github.com/electrickite/luks-tpm2)

