---
title: 'EFI Preparation'
taxonomy:
    category:
        - docs
---

These explanations and commands are quoted from the [oroginal topic][1]

> The first step is enabling support for Secure Boot. Normally it should be disabled because if using its default settings it prevents Manjaro from booting, but as it is required for ensuring that only allowed code is executed, you need to enable it as follows:

Breaking down the steps
1. create a folder to hold the keys
2. remove permissions for other than root

```

# mkdir -p -v /etc/efikeys
# chmod -v 700 /etc/efikeys
# cd /etc/efikeys
```

> The following saves default Secure Boot keys just in case, but we’re not going to change / remove them anyway so you can skip this step:

__Note:__ If you get a message __Variable XX has no entries__ don't worry - it just means there is no values to backup - in my case it is because the laptop was bought without the Windows operating system.

The steps outlined is copying values from efi to a file
```

# efi-readvar -v PK -o default_PK.esl
# efi-readvar -v KEK -o default_KEK.esl
# efi-readvar -v db -o default_db.esl
# efi-readvar -v dbx -o default_dbx.esl
```

> Now it’s time to generate machine-specific key which will be used to authenticate your Manjaro kernels and bootloader(s):

Still inside the `/etc/efikeys` folder.

1. Create a certificate request  using the system hostname with a validity of 10y
2. Generate a selfsigned certificate based on the request

```

# openssl req -new -x509 -newkey rsa:2048 -subj "/CN=$(hostname) platform key/" -keyout db.key -out db.crt -days 3650 -nodes -sha256
# openssl x509 -outform DER -in db.crt -out db.cer
```
> Now let’s copy our Secure Boot certificate on $ESP to let MokManager utility enroll it on the next boot

Now we need a new root folder for the $esp 
```
# mkdir -p /efi
```
Mount the $esp partition - usually the first partition on the device e.g. `/dev/sda1` or /`dev/nvme0n1/p1`
```
# mount /dev/nvme0n1p1 /efi
```

Still in the `/efi/efikeys` folder the certificate must copied to the $esp for MOK manager to use on next boot.
```
# cp db.cer /efi/MOK.cer
```
__NOTE__: I have vague recollection of systemd-boot possibly failing on kernel update when  $esp is not mounted at /boot - I will have to watch this part closely.

Change fstab mountpoint to `/efi
```
# sed -i 's|/efi/boot|/efi|' /etc/fstab
```

Reference
---

__Note__: as I am building from scratch with systemd-boot this is for reference only.

> Now you need to replace grub with systemd-boot to make whole setup easier and more straightforward. With extra effort you can keep Grub and use it as your bootloader but that again is out of scope of this how-to since it would overcomplicate things dramatically. Frankly speaking, systemd-boot isn’t necessary too, but it makes booting different kernels and efi tools easier than just UEFI boot menu.
>
> First, remove grub:
> ```
> # pacman -R grub
> ```
> The following will change the mountpoint of your EFI partition to sane and logical location, which is, most importantly, perfectly supported by systemd-boot.

Breaking down the command
1. unmount the efi partition
2. change the mount in fstab
3. create a new mountpoint `/efi`
4. remount all from fstab if not mounted

> ```
>
> # umount /boot/efi
> # sed -i 's|/efi/boot|/efi|' /etc/fstab
> # mkdir /efi
> # mount -a
> ```





[1]: https://forum.manjaro.org/t/howto-use-tpm2-to-unlock-luks-partition-on-boot/101626