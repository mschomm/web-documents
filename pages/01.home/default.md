---
title: Home
published: true
date: '01-10-2019 00:00'
publish_date: '01-10-2019 00:00'
metadata:
    author: linux-aarhus
---

## My notes on Linux and software development.

I don't think an efficient system exist - unless you build it ground up the Arch way.

If you choose any distribution you are presented with an initial layout defined by the philosophy driving the developers.

EndeavourOS is one the best Arch derivatives for this purpose.

## linux experience
I have been touching down from time to time on Linux for decades but due to my professional life concentrated around clients with Windows environments - a few with macOS mixed in - and therefore it has been difficult to get a firm grip on Linux.

All my touch downs with Linux in the past was very often accompanied by graphic driver issues. My atttempts to learn was often worked against due to lack of resources that could be dedicated to Linux.

The incoherent appearance of the confusing mix of gtk and early qt was also turnoff.

The early attempts on using linux requiring extensive knowledge of compiling software from source made me turn to OS/2 with were better but with no tools.

So I found myself going back to Windows again and again. My earliest experiences was Clark Connect Firewall/Gateway/Proxy and Suse Linux Enterprise edition with SLOX. But again the pain of configuring the hardware - the slow internet connection - my lack of knowledge in C - all stalled my progress.

It was only when I learned about Arch around 2011 and learned how to put the Linux puzzle together - how the filesystem holds different pieces - the filesystem concept of _everything is a path_ even devices - where the local system wide configs are stored - how the configs are prioritized from /usr/ -> /etc -> ~/.config and ~/.local - that is were it began to make sense.

When the great picture dawning I began looking for an easier way to install arch and my first experience with an Arch derivative was Cinnarch.

I really liked Antergos Gnome edition and stayed with Gnome for a long time. Some of you may remember that Arch has not always been as stable as it is now and back in the early Antergos days - Antergos was not that stable either.

## memory lane
I discovered Manjaro back in 2015 and oddly enough given the very early history of Manjaro (which I have not experienced first hand) my Linux experience stabilized - I still don't know why - but it did.

Despite my Python knowledge was null and void - I volunteered when a team member asked for help in rewriting the pacman-mirrors code - so I started learning Python - I am by no means an expert - and I willing to admit I rely a lot on research when I code - the topic is always covered - perhaps not in details but generic examples on how to solve a challenge is often invaluable in the learning process. 

The April fools day - where the team created the prank to drop x86 and concentrate on ARM - I got invited to join the team - I hesitated - I didn't want to be part of prank - and despite my coding skills in deprecated languares I had no desire to be part of a distribution - but I was flattered that a no-one - a blank sheet - like me would be considered so - the flattery worked - I got a manjaro email address.

That was a steep learning experience - while I could draw on the core team if I had questions - asking for help is not my strong side - I am a stubborn idiot who takes pride in learning by reading and doing so I decided to figure out everything on my own.

So I did figure out how to build the settings packages - how to follow the tracks down through PKGBUILD after PKGBUILD to locate the source - then learn to modify and created new PKGBUILD and then use those to build an ISO and that was done without any documentation - other than reading a collection of undocumented bash scripts - learning how ENVIRONMENT variables work when running scripts - so my know very deep knowledge of inner workings of an Arch based distribution - I have accumulated through the experience of being a member of the Manjaro Team.

This resulted in the resurrection of the Manjaro Openbox ISO and builing that has been very, very educational.

In 2022 I retired from being a part of maintaining the distribuition.


## final reflection
So an efficient ISO is impossible to build as it will only match the select few who shares your opinion on efficiency.

This is the force of Arch - and Arch or an Arch based distribution without to much bias can be used to create your own exeperience - you create your own efficiency and control your workflow down to the sligthest detail.

This is also why complete beginners should never - unless ready to do some university style study and dedicate themselves to the process - start with Arch or an Arch derivative.

## _The distro_ doesn't add features, and the features it does add I don't want
Originally posted by Jonathon Fernyhough at [Archived Manjaro forum - April 19, 2019][1]
> 
> I want Manjaro to add a feature which I want. I can't do it myself but it's really important to what I currently think is important so Manjaro should add it.
> 
> The developers are lazy because they don't do what I want them to do, but they still find time to add features which other people ask for. That's stupid and lazy.
> 
> Those features which other people want aren't what I want, so they are a waste of time and completely bloat.
> 
> I hate bloat. Bloat is extra features that I don't want. Not like the extra features which I do want because they aren't bloat because I want them.
> 
> Why does Manjaro include stuff which I don't want? Why won't Manjaro include what I want instead of what other people want?
> 
> Why can't Manjaro be made into what I want, and also what everyone else at the same time wants because they also want what I want (apart from the people who don't want because they are noob and hate privacy)?
> 
> I keep asking for someone else to do something for me over and over and over again and yet they still haven't done what I want them to. This means Manjaro is terrible and shouldn't be used by anyone, ever. I never liked Manjaro anyway because it's made by script kiddies, but I'm doing this because I want Manjaro to be popular because if it did what I wanted then it would be more popular and more people would want to use it because everyone wants the same thing that I want.
> 
> It just shows that because you don't do what I want that you don't know what you're doing and I'm never going to recommend Manjaro to anyone.
> 
> But if you did do just this one thing which I want then Manjaro would be the best.


[1]: https://archived.forum.manjaro.org/t/manjaro-doesnt-add-features-and-the-features-it-does-add-i-dont-want/83220