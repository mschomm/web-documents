---
title: 'Packagelist and Configuration backup'
taxonomy:
    category:
        - docs
---

Difficulty: ★☆☆☆☆

Use the script to backup important config.

* create list of explicit installed packages
* backup common configuration to archive

Use case - transfer common configuration and package list to other systems

https://wiki.manjaro.org/index.php/Important_hidden_.dot_files_in_your_home_partition

```
#! /bin/bash
#
# Script for backing up configuration and package lists
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the Affero GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# @linux-aarhus - root.nix.dk
#

# ##########################################################
# Modify as necessary

# example filelist=('.bash_profile' '.bashrc' '.netrc' '.profile' '.zshrc')
filelist=('.bash_profile' '.bashrc' '.netrc' '.profile' '.zshrc')

# example folderlist=('.config' '.local' '.gnupg' '.mozilla' '.ssh' '.thunderbird')
folderlist=('.config' '.local')

# configuration file name
archive_file="dotconf.tar.gz"

# official repo package list
repo_pkg_file="repo-pkglist.txt"

# custom package list
cust_pkg_file="cust-pkglist.txt"

# Do not edit below this line - unless you know what you are doing.
# ##########################################################

SCRIPTNAME=$(basename "$0")
VERSION="0.2"
if [[ -z $1 ]]; then
    echo ":: $SCRIPTNAME v$VERSION"
    echo "==> missing argument: PATH"
    echo "Usage:"
    echo "  $SCRIPTNAME /path/to/backup"
    echo "  Path to store output"
    echo "  e.g. $SCRIPTNAME /home/$USER/backup"
    echo ""
    exit
fi

set -e

if ! [[ -d $1 ]]; then
    mkdir -p $1
fi

conf_archive="$1/$archive_file"
repo_pkg_list="$1/$repo_pkg_file"
cust_pkg_list="$1/$cust_pkg_file"

# create an archive of common hidden files and folders

if [[ -e "$conf_archive" ]]; then
    # remove archive if exist
    rm -f "$conf_archive"
fi

todo=""
for file in ${filelist[@]}; do
    if [[ -f $file ]]; then
        todo+="${file} "
    fi
done

for folder in ${folderlist[@]}; do
    if [[ -d ${folder} ]]; then
        todo+="${folder} "
    fi
done

tar -zcvf "$conf_archive" $todo

# list packages from official repo
pacman -Qqen > "$repo_pkg_list"

# list foreign packages (custom e.g. AUR)
pacman -Qqem > "$cust_pkg_list"

echo " ==> Packagelists created"
echo "   --> $repo_pkg_list"
echo "   --> $cust_pkg_list"
echo " ==> Config archive created"
echo "   --> $conf_archive"
echo " ==> To install packages from lists"
echo "   --> sudo pacman -Syu --needed - < $repo_pkg_file"
echo " ==> To restore the configuration files run"
echo "   --> tar -xzf --overwrite -C $HOME $archive_file"
echo ""
```

## Usecase

* you own and operate computer A with your configuration
* you now aquired a computer B which you will install with Manjaro
* apply the same configuration on computer B as you have on computer A
* in this context configuration is the files and folders found in
  *  $HOME
  *  /home/$USER
    
**ONLY user configuration is supported**

Edit the script variable section at top - save an execute

**On computer A**
The script creates three (3) files in the directory you specify - e.g. an USB stick

* a list of repo packages
* a list of custom packages
* a tarred archive with the configuration

**On computer B**
Navigate to the USB stick and open a terminal

Install repo packages

```
sudo pacman -Syu $(repo-pkglist.txt)
```

Install custom packages

```
pamac build --no-confirm $(cat cust-pkglist.txt)
```

Extract the files

```
tar -xzf --overwrite ---directory $HOME dotconf.tar.gz
```
Logout for the changes to apply everywhere

**Caveats**
This script does not take into account applications added using flatpak, snap or appimage.

Please consult the applicable man pages on how to list such applications and subsequently add such packages to the new system.

User configuration pertinent to those packages likely exist in either ~/.config and ~/.local but may exist in other locations inside your home.