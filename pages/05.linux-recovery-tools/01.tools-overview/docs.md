---
title: 'Tools Overview'
taxonomy:
    category:
        - docs
---

## packages
| Tool      | Download              |
|   ---     |       ---             |
| ddrescue  |  pacman -S ddrescue   | 
| testdisk  |  pacman -S testdisk   |
| photorec  |  part of __testdisk__ |
| foremost  |  pacman -S foremost   |
| sleuthkit |  pacman -S sleuthkit  |
| r-linux   |  pamac build r-linux  | 
| mondo     |  pamac build mondo    |
| safecopy  |  pamac build safecopy |

## bootable systems
| system | download |
|  ----  | ---      |
| redo rescue (debian based ISO) | http://redorescue.com/ |
| sysrescd (arch based ISO) | https://www.system-rescue.org/ |
| knoppix (ISO) | http://knoppix.net/  |
| trk3 (ISO) | https://sourceforge.net/projects/archiveos/files/t/trk |     
| rescatux (ISO) | https://www.supergrubdisk.org/rescatux/

## windows bootable
| system | download |
|  ---- | --- |
| Hirens Boot CD | https://www.hirensbootcd.org/files/HBCD_PE_x64.iso |
| Hirens Boot CD Legacy | https://www.hirensbootcd.org/files/Hirens.BootCD.15.2.zip |
