---
title: 'Computer doesn''t boot - black screen or message'
date: '12:18 12-01-2023'
taxonomy:
    category:
        - docs
---

Originally written by Jonathon Fernyhough at Garuda Linux forum - for reference

[jonathon | 2022-06-19 11:27:30 UTC | #1][1]

While there are many potential reasons why your installation doesn’t boot to a desktop, the primary reason is that the graphics drivers haven’t loaded correctly so your login/display manager (DM) doesn’t start. This is why you get a “black screen” or stuck at an

```
[   OK   ] Started some service
```

message.

The normal reason for this is that you have the incorrect driver version for the kernel. This can happens when the mirror you are updating from is not fully synchronised, or times out, and so you have different sets of packages in `[core]` and `[extra]`. This means the driver module package in `[extra]` has been built for a different kernel point-release to that in `[core]`. **On the other hand, if you have a DKMS driver package then make sure the build succeeded!**

# The first thing to try

Switch to a text console with <kbd>CTRL</kbd>+<kbd>ALT</kbd>+<kbd>F2</kbd>, log in with your normal username and password, then run a full update:

```
sudo reflector -a6 -f5 --save /etc/pacman.d/mirrorlist
garuda-update
```

The first command will refresh your mirror list, picking five of the most recently updated mirrors (so those mirrors will all be up-to-date).

The second command will run Garuda's update script which takes care of many update issues.

If any packages were updated, reboot.

**Note: If you can’t log in to a text console, use a [chroot ](https://forum.garudalinux.org/t/how-to-chroot-garuda-linux/4004?u=jonathon) instead.**

# The second thing to try

Log into a text console as above and run `startx` . This should either start X, in which case the issue lies with your Display Manager (e.g. GDM, LightDM, SDDM), OR you will be presented with some output and returned to the console. Of particular interest are Error (EE) lines.

# The DKMS thing to try

Run:

```
sudo dkms autoinstall
```

and pay attention to the build completion status. If it says the build failed then check the associated log file (it will tell you a file location).

# If you can’t see anything obvious

If none of the above helps fix this issue (which the first one will in 90% of cases) then search all Arch OS related forums for recent issues (https://forum.garudalinux.org/search?q=black%20screen), and, if you can’t find anything, start a new thread mentioning what you have already tried.


[1]: https://forum.garudalinux.org/t/computer-doesn-t-boot-boots-to-a-black-screen-or-stops-at-a-message/2953