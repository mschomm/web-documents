---
title: 'URL pastebins by @cscs'
date: '07:09 18-12-2023'
taxonomy:
    category:
        - docs
---

Our friend on Manjaro Linux Forum [@cscs][1] has a collection of pastebin services

Sometimes we might want to easily share terminal output.

While this can be accomplished directly in the forums or manually through websites like pastebin, there also exist a number of open services that allow you to paste and share directly from the command line by using tools you already have installed.

This is great for many situations, including troubleshooting from a non-graphical boot, or simply very large outputs. Here I will share a few examples and how to use them.

**Services and Examples:**

[0x0.st](https://0x0.st/)
>```
><command> | curl -F 'file=@-' https://0x0.st
>```

[rustypaste](https://rustypaste.shuttleapp.rs/)
>```
> <command> | curl -F 'file=@-' https://rustypaste.shuttleapp.rs
>```

[paste.c-net.org ](http://paste.c-net.org)
>```
><command> | curl -s --data-binary @- 'https://paste.c-net.org/'
>```

[clbin.com](https://clbin.com/)
>```
><command> | curl -F 'clbin=<-' https://clbin.com'
>```

[ix.io ](http://ix.io) (is taking a break for the time being)
>```
> <command> | curl -F 'f:1=<-' ix.io'
>```

[sprunge.us ](http://sprunge.us) 
>```
> <command> | curl -F 'sprunge=<-' http://sprunge.us'
>```

**Aliases and other tricks:**

To *quasi-install* [rustypaste](https://rustypaste.shuttleapp.rs/) pasting and sharing add the following to your bashrc (zshrc untested):

[details="paster .bashrc"]
```
paster()
{
    local url='https://rustypaste.shuttleapp.rs'
    if (( $# )); then
        local file
        for file; do
            curl -F "file=@""$file""" "$url" 
        done
    else
        curl -F 'file=@-' "$url"
    fi
}
```
[/details]
Then you can use `paster`:
To share outpot
```
<command> | paster
```
To shar file(s)
```
paster <filepath> <filepath2>
```


To *quasi-install* [paste.c-net.org ](http://paste.c-net.org) pasting and sharing add the following to your bashrc (zshrc untested):
[details="paste.cnet.org .bashrc"]
```
pastenet()
{
    local url='https://paste.c-net.org/'
    if (( $# )); then
        local file
        for file; do
            curl -s \
                --data-binary @"$file" \
                --header "X-FileName: ${file##*/}" \
                "$url"
        done
    else
        curl -s --data-binary @- "$url"
    fi
}
pasteget()
{
    local url='https://paste.c-net.org/'
    if (( $# )); then
        local arg
        for arg; do
            curl -s "${url}${arg##*/}"
        done
    else
        local arg
        while read -r arg; do
            curl -s "${url}${arg##*/}"
        done
    fi
}
```
[/details]

Then you can use 'pastenet' and 'pasteget' a few ways:
To share output
```
<command> | pastenet
```
To share file(s) with a 50M limit
```
pastenet <filepath> <filepath2>
```
To download paste.c-net shared content
```
pasteget https://paste.c-net.org/<exampleurl>
``` 

**Notes and Warnings**

* There is more detailed info at each host, including options like setting expiration time and more.
* Please [dont abuse](https://github.com/ptpb/pb/issues/246) these services. They are made freely available, so dont knock a good thing.
* Further stipulations, user and privacy clauses and the like can be found at each project host.
* Remember to avoid sharing personal information. 
(see this related page for tips: https://forum.manjaro.org/t/how-to-provide-good-information/874)

-------------------------

[1]: http://cscs.gitlab.io/
