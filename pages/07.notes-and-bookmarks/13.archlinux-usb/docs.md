---
published: true
date: '01-10-2019 00:00'
publish_date: '01-10-2019 00:00'
title: 'ArchLinux USB'
taxonomy:
    category:
        - docs
metadata:
    author: linux-aarhus
---

### Tips running of an USB

https://magyar.urown.cloud/arch-usb.html <sup>dead link</sup>
Archive [https://uex.dk/archive/docs/arch-usb][1]

### Secure boot
* Signed shim [https://aur.archlinux.org/packages/shim-signed/][2]
* Setup [https://wiki.archlinux.org/index.php/Secure_Boot#Set_up_shim][3]
### Interface names
Interface names
```
# ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules 
```
Alternaive tools for network configuration
```
netctl
ifplugd
iw
iwd
```
sample profiles in `/etc/netctl/examples`

Journal config
```
# nano /etc/systemd/journald.conf
```
```
Storage=volatile
SystemMaxUse=16M
```

Drivers
```
xorg-drivers
```

[1]: https://uex.dk/archive/docs/arch-usb
[2]: https://aur.archlinux.org/packages/shim-signed/
[3]: https://wiki.archlinux.org/index.php/Secure_Boot#Set_up_shim
