---
title: 'Dummy notes for Arch install'
published: true
taxonomy:
    category:
        - docs
date: '2020-09-15 10:57'
publish_date: '2020-09-15 10:57'
---

Keyboard

```bash
# loadkeys dk
```

System time

```bash
# systemctl enable --now systemd-timesyncd
```

Clear disk method one
```bash
# cfdisk --zero /dev/sda
```
Clear disk method two
```
# sgdisk --zap-all /dev/sda
```

EFI ($esp)
```bash
# mkfs.fat -F 32 /dev/sda1
```
Swap
```bash
# mkswap /dev/sda2
```
Root
```bash
# mkfs.ext4 /dev/sda3
```
Home
```bash
# mkfs.ext4 /dev/sda4
```
Mounting
```bash
# mount /dev/sda3 /mnt
# mkdir -p /mnt/boot/efi
# mkdir -p /mnt/home
```

Verify your folder structure
```bash
# find /mnt -type d
/mnt
/mnt/home
/mnt/boot
/mnt/boot/efi
```

Mount the efi and home partition
```bash
# mount /dev/sda1 /mnt/boot/efi
# mount /dev/sda4 /mnt/home
```


Base installation
```bash
# pacstrap /mnt base linux dhcpcd vi nano sudo links
```

Base configuration
```bash
# archchroot /mnt /bin/bash
```
 vconsole.conf
 ```bash
KEYMAP=dk
FONT=
FONT_MAP=
```

locale.gen
```bash
...
da_DK.UTF-8 UTF-8
en_DK.UTF-8 UTF-8
en_US.UTF-8 UTF-8
...
```
Build locale
```bash
# locale-gen
```
locale.conf
```bash
LANG=en_DK.UTF8
```
Timezone
```bash
# ln -sf /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime
```

Clock
```bash
# hwclock --systohc --utc
```

Set hostname
```bash
# echo nix > /etc/hostname
```

hosts
```bash
127.0.0.1    localhost
::1          localhost
127.0.1.1    nix.localdomain nix
```
Note: If the system has a static IP replace 127.0.1.1 with the IP.

Enable wheel group
```bash
%wheel ALL=(ALL) ALL
```

Enable network
```bash
# systemctl enable dhcpcd
```

Enable timesync daemon
```bash
# systemctl enable systemd-timesyncd
```

root password
```bash
# passwd
```

Install grub mkinitcpio efi boot manager
```
¤ pacman -S grub mkinitcpio efibootmgr
```

Build the initramfs

```bash
# mkinitcpio -p linux
```

install bootloader
```bash
# grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Manjaro
```

Generate grub configuration

```bash
# grub-mkconfig -o /boot/grub/grub.cfg
```
Exit chroot
```bash
# exit
```
Unmount
```bash
# umount -R /mnt
```
Restart (remember to remove your install media)
```bash
# reboot
```
Login as root and test your internet connection
```bash
$ links root.nix.dk
```
