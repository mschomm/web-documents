---
title: 'Time Calendar Weather widget'
taxonomy:
    category:
        - docs
published: false
date: '01-10-2019 00:00'
publish_date: '01-10-2019 00:00'
---

## Description
The widget displays date and time and network info with weather information for your location.

The widiget uses OpenWeatherMapApi which provides a nice api for getting your local weather and it provides the api for private usage - free of charge.

## Requirements
To use this widget you are required to register at [openweatherapi.org][1]. The widget displays the information using your system locale - if available - otherwise defaulting to english 

The date and time part of the conky uses lua script so the conky binary must have lua support.

Ensure you have the necessary packages installed
```
$ sudo pacman -Syu git base-devel --needed
```

* Build instructions
   ```
   $ git clone https://aur.archlinux.org/conky-lua.git
   $ cd conky-lua
   $ makepkg -is
   ```
The widget is arranged below the conky folder 
```
$ tree ~/.config/conky
.
├── clock
│   └── KvFlatRed.lua
├── KvFlatRed.conkyrc
└── openweather
    ├── cw
    ├── cx-icon.json
    ├── cx-lang.json
    └── fetcher
```


[1]: https://home.openweathermap.org/users/sign_up