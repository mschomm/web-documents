---
published: false
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'OpenWeather widget'
taxonomy:
    category: docs
---

OpenWeather provides a nice api for getting your local weather and it provides the api for private usage - free of charge - only requirement is an API which get upon registration.

This widget makes use of the api to pull the weather for a postcode and country. It displays the weather description in a language of choice so it is possible to get a localized string for select languages.

The widget is consist of a python script, a crossreference for the actual weather icon and the conky script.

