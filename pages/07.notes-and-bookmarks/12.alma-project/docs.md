---
published: true
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'ALMA project'
taxonomy:
    category:
        - docs
metadata:
    author: linux-aarhus
---

## Arch Linux Mobile Appliance

[ALMA Github][1]

Project to create a hybrid USB with perstistence.

[1]: https://github.com/r-darwish/alma
