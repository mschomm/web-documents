---
title: 'Decrapify Android Phone'
taxonomy:
    category:
        - docs
published: true
date: '2020-10-28 07:28'
publish_date: '2020-10-28 07:28'
---

## How to decrap a Android phone

* Enable developer mode
* Enable android debug
* Launch adb shell

[ADB Shell reference][1]

List packages

    pm list packages -f | grep 'com.huawei'

Uninstall named package

    pm uninstall —user 0 tld.domain.pkg.apk
    
To keep data

    pm uninstall -k --user 0 tld.domain.pkg.apk


[1]: https://adbshell.com/commands/adb-shell-pm-list-packages