---
title: 'Sample mount units'
published: true
taxonomy:
    category:
        - docs
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
metadata:
    author: linux-aarhus
---

## Example mount units for systemd
---
## Unit file names
---
### Mount unit
* Mount units must be named the mount point with the extension of .mount
  e.g. mountpoint */data/backup* - unit file *data-backup.mount*

### Automount unit
* Automount units use the name of the mount unit but an extension of *.automount*
  e.g. *data-backup.automount*

## Content of a mount unit
---
Depending on the **<kbd>Type</kbd>**, the **<kbd>What</kbd>** is different and so is the **<kbd>Options</kbd>**

* **<kbd>What</kbd>** do you want to mount (disk, server, share)?
    
**<kbd>Where</kbd>** do you want it to mount (a path on your system)?
    
Which **<kbd>Type</kbd>** is your mount (filesystem, cifs, nfs)?
    
What **<kbd>Options</kbd>** should the mount use(rw, auto, netdev)?

### Example
If you want to mount a Samba share named **video** provided by a local NAS named **server**

* **<kbd>What</kbd>** is the name of server and the share e.g. **//server/video**
* **<kbd>Where</kbd>** is the path where you can browse the data provided by the share e.g. **/data/smb/video**
* **<kbd>Type</kbd>** will be **cifs** because that is what this type of connection is called
* **<kbd>Options</kbd>** will instruct the system it is a net device **_netdev**, it should be possible to read and write **rw**, and connect using a specific **workgroup**, **username** and **password**.

## AUTOMOUNT units
---
Same rule applies as for mount units. It must be the path to the mountpoint with the *.automount* extension e.g. **data-backup.automount**


[1]: http://curlftpfs.sourceforge.net/
[2]: https://wiki.archlinux.org/index.php/CurlFtpFS
[3]: https://wiki.archlinux.org/index.php/samba
