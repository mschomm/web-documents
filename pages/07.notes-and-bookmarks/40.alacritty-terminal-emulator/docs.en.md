---
title: 'Alacritty Terminal Emulator'
taxonomy:
    category:
        - docs
---

Alacritty terminal dimensions

---
```
document:
    name: ~/.config/alacritty/alacritty.yml  
	type: configuration
```
---

```
window:
    dimensions
        columns: 120
        lines: 40
```

