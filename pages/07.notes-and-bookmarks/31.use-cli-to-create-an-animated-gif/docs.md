---
title: 'Use CLI to create an animated GIF'
taxonomy:
    category:
        - docs
---

```
convert -delay 100 -loop 0 *.png ani.gif
```