---
title: 'Capture terminal output to clipboard'
taxonomy:
    category:
        - docs
---

Install `xclip` and/or `xsel` packages

Using `xclip`
```
cat filename.txt | xclip -sel clip
```
Using `xsel`
```
cat filename.txt | xsel -ib
```