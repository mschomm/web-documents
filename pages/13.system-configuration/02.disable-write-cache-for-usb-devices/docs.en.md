---
title: 'Disable write-cache for USB devices'
date: '05:49 13-04-2023'
taxonomy:
    category:
        - docs
---

## Linux filesystem cache

Linux agressively caches files in order to improve overall performance.

When copying large amount of files to an USB storage this often results in some wait time until the device can be safely removed.

How long you have to wait depends on your system and the quality of the USB storage device.

Numerous issues regarding this has resulted in various suggestions involving sysctl tweaks and trim.

One example is this topic https://forum.manjaro.org/t/decrease-dirty-bytes-for-more-reliable-usb-transfer/120798

## udev rule to disable write-cache

In [another place][2] one user threw in an udev rule which would disable write-cache for devices when they were added and while it was just an idea - it triggered my curiosity.

I dug into the intricacies of udev and found a method to only target USB storage devices.

* Documentation for https://wiki.archlinux.org/title/udev

The final rule 
```

# rule to disable write cache for usb storage
# requires hdparm to be installed
ACTION=="add|change", KERNEL=="sd[a-z]", ENV{ID_USB_TYPE}=="disk", RUN+="/usr/bin/hdparm -W 0 /dev/%k"
#
# the following rules is introduced with kernel 6.2
# https://docs.kernel.org/admin-guide/abi-testing.html#abi-sys-class-bdi-bdi-strict-limit
# https://docs.kernel.org/admin-guide/abi-testing.html#abi-sys-class-bdi-bdi-max-ratio
# https://docs.kernel.org/admin-guide/abi-testing.html#abi-sys-class-bdi-bdi-max-bytes
ACTION=="add|change", KERNEL=="sd[a-z]", ENV{ID_USB_TYPE}=="disk", RUN+="/usr/bin/echo 1 > /sys/block/%K/bdi/strict_limit",  RUN+="/usr/bin/echo 50 > /sys/block/%K/bdi/max_ratio", RUN+="/usr/bin/echo 16777216 > /sys/block/%K/bdi/max_bytes"

```

The rule activates on

* add or change
* kernel event for disk devices **sd[a-z]**
* only if the device environment **ID_USB_TYPE=='disk'**
* run 
* run
   - hdparm -W 0 /dev/%K
   - /usr/bin/echo 1 > /sys/block/%K/bdi/strict_limit
   - /usr/bin/echo 50 > /sys/block/%K/bdi/max_ratio
   - /usr/bin/echo 16777216 > /sys/block/%K/bdi/max_bytes


Create a file in **/etc/udev/rules.d/99-usb-sync.rules** and paste above content into the file and save it.

Install **hdparm** package.

    sudo pacman -Syu hdparm

Then plug an usb device - open in your file manager - copy a huge amout of files to the device - when the copy is done - click eject in the file manager - note how quick the device is ejected.

For those preferring the package manager, I have created a [PKGBUILD][1] which will pull the **hdparm** dependency upon installation.
```
pamac build udev-usb-sync
```

PKGBUILD source at [https://codeberg.org/wonky/udev-usb-sync][3]

[1]: https://aur.archlinux.org/packages/udev-usb-sync
[2]: https://forum.endeavouros.com/t/how-come-everything-is-written-when-i-decide-to-unmount-a-usbdrive/37178/10
[3]: https://codeberg.org/wonky/udev-usb-sync