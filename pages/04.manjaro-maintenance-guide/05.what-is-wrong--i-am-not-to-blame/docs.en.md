---
title: 'What is wrong - I am not to blame'
date: '09:50 02-11-2022'
taxonomy:
    category:
        - docs
---

linux-aarhus | 2020-10-09 15:24:53 UTC | Source: [What is wrong - I am not to blame](https://forum.manjaro.org/t/what-is-wrong-i-am-not-to-blame/30565)

When you're updating your system and you have an issue with any installed package installed - your immediate thought is - the update is at Manjaro fault.

**No - it is not** - you are

While Manjaro provides the updates the result is yours.

You may think - I am safe using the updates provided.

No - you are not - whatever packages installed using AUR may need to be reinstalled - other packages may not be compatible with your installation.

Yes you can install a lot of packages which in various ways will interfere with your update but that is not to be blamed on Manjaro.

---

> [Comment on comment](https://forum.manjaro.org/t/what-is-wrong-i-am-not-to-blame/30565/9)
> 
> Why is this an announcement? This seems condescending at best and flamebait at worst. -- [lectrode](https://forum.manjaro.org/t/what-is-wrong-i-am-not-to-blame/30565/8)

There is no  ill intentions what so ever - but seeming I have touched a nerve - but the intention - putting this in the announcement section is to draw attention to and to emphasize the point and

**The point being:** 

There is so many combinations possible - hardware and software - and you really need to understand - while Manjaro provides the system and as such also updates it is

- impossible to cover every single system combo
- there is **no guarantee** every possible combo will work

With the increasing popularity of Manjaro - there is an increasing tendency to expect that any changes made to the system will not be become affected by updates and this expectation is fundamentally wrong.

Every possible system will deviate on almost every aspect when compared to the systems used by the Manjaro team thus making it next to impossible to create the *perfect* system and every Manjaro user must have a basic understanding of Linux - especially how the Arch based Linux works.

---
> [Comment on comment](https://forum.manjaro.org/t/what-is-wrong-i-am-not-to-blame/30565/31)
> 
> That being said, you all desperately need to hire someone to handle your public relations. Some of your posts come off so “user hostile”. I’m not sure why you chose to take this tone with the community that is supporting your work. You could have easily said all of this in a much more palatable manner. -- [Jojo_phet](https://forum.manjaro.org/t/what-is-wrong-i-am-not-to-blame/30565/27)

You may be right - my apology - I am not very good at PR - what I am good at is system maintenance and coding.

The initial target is not the member's of the community - it is targeted those - with an attitude - those demanding solutions and casting blame - those are the target - not you - not the member on a learning path.

The intention with this is to raise awareness - to make new members self-aware - aware of the possibility that some system combinations which used to work - may cease to work and when this happens - it is not the fault of Manjaro - but could as well be the combination of factors.

Manjaro is a spare time project - a very successful project - but that doesn't change the fact that it is still only backed by a few people in their spare time and users new to the Linux ecosystem tend to forget that if something is easy - it is because some other user has put a tremendous amount of time into *making it easy*.

What I refer to as basics is basic Linux knowledge. Linux is not Windows - and you cannot apply the logic from Windows onto your Linux.

We had a short article in the FAQ section on the archived forum

* https://archived.forum.manjaro.org/t/linux-windows-why-should-linux-want-me/126641

The following statement is true

> Manjaro is suitable for both newcomers and experienced computer users. - https://manjaro.org/

There has been comparison with vehicles and this is a good comparison - because you choose vehicle - according to the use case - maybe even the terrain you are in. While you can expect your Tesla or your Rolls Royce to be flawless - it is simply because you have paid a lot of money for it.

When you buy a new vehicle you don't start by customizing the look - rearranging the intererior - moving the seats around - removing doors or moving them around because you want the hinges on the other side or you want that rear door to open the other way - just because you heard it was possible. You start using the vehicle for the purpose it is intended. You don't try to change it into something it is not - you have realistic expectations to what it is capable of - because you research before you made the investment.

> Unlike proprietary operating systems, you have full control over your hardware, without restrictions. This makes it ideal for people who want to learn how Linux works and how it is different to other operating systems. From this perspective, it is also suitable for beginners similar to the way an Arduino is an excellent entry-point to embedded hardware development. - https://manjaro.org/

Putting this perspective onto Linux - you have not paid a dime. But most immediately begin to rearrange the interior. Experimenting with different kinds of software - all with out having any basic knowledge.

When you buy a system with Manjaro pre-installed you are entitled to higher expectation as the Manjaro system may have been tuned and adjusted for this specific hardware.

But if you alter the parameters - and you will - it is inevitable - situations will come where you need to backtrace what may cause this issue to rise as Manjaro cannot take into account all those small changes which one on its own is nothing but a 100 is not. 

Manjaro - any Linux system - with Nvidia Optimus graphics is in the mercy of the vendor. Linux world is doing a good job in adjusting and trying to keep up but you can never point at Manjaro and say - you are at fault here - simply because for the 100 hardware identical laptops - there will be a 100 configurations as every user is different. Even though the laptops are identical - the hardware components are not. They may be from different batches - some basic electronic components may even be from different vendors depending on the current price.

In combination with the any given user preference - this makes it impossible to guarantee that any given combination of user/software/hardware will ever work flawless.

-------------------------